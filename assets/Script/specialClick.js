// 特殊点击事件
// 长按 + 双击

cc.Class({
    extends: cc.Component,
    properties:
    {
        longTouch : {
            default : false,
            tooltip : "长按开关",
        },
        longTouchTimeMin : {
            default: 2000,
            tooltip : "长按最短响应时间（ms）",
            visible() {
                return this.longTouch;
            }
        },
        longTouchEvent : {
            default: null,
            tooltip : "长按回调事件",
            type : cc.Component.EventHandler,
            visible() {
                return this.longTouch;
            },
        },
        doubleClick : {
            default : false,
            tooltip : "双击开关",
        },
        doubleClickTimeMin : {
            default: 400,
            tooltip : "双击最短间隔时间（ms）",
            visible() {
                return this.doubleClick;
            }
        },
        doubleClickEvent : {
            default: null,
            tooltip : "双击回调事件",
            type : cc.Component.EventHandler,
            visible() {
                return this.doubleClick;
            },
        },

        // 长按触摸状态
        _longTouchState : false,
        // 长按触摸时长
        _longTouchTime : 0,
        // 双击状态
        _doubleClickState : false,
        // 双击间隔时间
        _doubleClickTime : 0,
    },

    // LIFE-CYCLE CALLBACKS:
    ctor()
    {
        //初始化事件
        this.longTouchEvent = new cc.Component.EventHandler();
        this.doubleClickEvent = new cc.Component.EventHandler();
    },

    onLoad () 
    {
        //触摸开始
        this.node.on(cc.Node.EventType.TOUCH_START,function(event){
            this._longTouchState = false;
            this._longTouchTime = 0;
        },this);

        this.node.on(cc.Node.EventType.TOUCH_END,function(event){
            //判断是不是长按
            if(this.longTouch)
            {
                if(this._longTouchTime >= this.longTouchTimeMin)
                {
                    //派发长按事件
                    this.longTouchEvent.emit([this.longTouchEvent.customEventData]);
                    this._longTouchTime = 0;
                    return;
                }
            }

            //判断是不是双击
            if(this.isDoubleClick)
            {
                if(this._doubleClickState)
                {
                     //第二次点击判断间隔时间
                    if(this._doubleClickTime <= this.doubleClickTimeMin)
                    {
                        //派发双击事件
                        this.doubleClickEvent.emit([this.doubleClickEvent.customEventData]);
                    }
                    this._doubleClickTime = 0;
                    this._doubleClickState = false;
                }
                else
                {
                    //第一次点击后，设置为双击状态，开始双击间隔时间计时
                    this._doubleClickState = true;
                    this._doubleClickTime = 0;
                }
            }
        },this);

    },


    update (dt)
    {
        //触摸计时
        if(this._longTouchState)
        {
            //防止这个数加的很大
            if(this._longTouchTime > this.longTouchTimeMin)
            {
                this._longTouchTime = this.longTouchTimeMin;
            }
            this._longTouchTime = this._longTouchTime + dt*1000;
        }
        //双击计时
        if(this._doubleClickState)
        {
            if(this._doubleClickTime > this.doubleClickTimeMin)
            {
                this._doubleClickTime = this.doubleClickTimeMin;
            }
            this._doubleClickTime = this._doubleClickTime + dt*1000;
        }
    },
});
