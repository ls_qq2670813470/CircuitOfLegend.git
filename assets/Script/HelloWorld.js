cc.Class({
    extends: cc.Component,

    properties: {

    },

    onLoad: function () {
        this.loadRes();
    },

    addGameLout()
    {
        var gameLayout = cc.instantiate(cc.globalPrefabs["gameLayout"]);
        gameLayout.parent = this.node;
    },

    // 预加载资源
    loadRes()
    {
        this._resCount = 0
        this._resLenght = 0;
        this._loadSuccess = false;

        //加载预制体资源
        var prefabs = {
            // 声音控制预制体
            "/game/prefabs/gameLayout": "gameLayout",
            "/game/prefabs/itemBase": "itemBase",
        };

        
        this._resLenght =  Object.keys(prefabs).length;
        this.loadGlobalPrefabs(prefabs);
    },

    // 预加载Prefab
    loadGlobalPrefabs(prefabs) {
        var that = this;
        cc.globalPrefabs = {};

        Object.keys(prefabs).forEach(function (key) {
            cc.loader.loadRes(key, cc.Prefab, function (err, prefab) {
                if (err) {
                    cc.error(err.message || err);
                    return;
                }
                cc.log('加载 prefab: ' + (prefab instanceof cc.Prefab));

                that._resCount++;
                cc.globalPrefabs[prefabs[key]] = prefab;

                that.loadResult(that, that._resCount, that._resLenght);
            });
        })
    },

    // 预加载资源结果
    loadResult(target, count, lenght) {
        //资源加载成功，跳转到登录界面
        if (count === lenght) {
            target._loadSuccess = true;
            target.addGameLout();
        }
    },

});
