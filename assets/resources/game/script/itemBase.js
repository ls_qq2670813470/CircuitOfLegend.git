
/**
 * 通道 上右下左
 */
var AISLE = cc.Enum({
    UP: 0,
    RIGHT: 1,
    DOWN: 2,
    LEFT: 3,
});

cc.Class({
    extends: cc.Component,

    properties: {
    },

    onLoad()
    {
        // 固定开关
        this.constant = true;

        // 旋转间隔角度
        this._rotation = 90;
        // 旋转状态[0,3]
        this.rotationState = 0;

        this.ctx = this.node.getChildByName("line").getComponent(cc.Graphics);
        this.ctxType = this.node.getChildByName("lineType").getComponent(cc.Graphics);

        var button = this.node.getComponent(cc.Button);
        button.node.on('click', this.callback, this);
        this.node.rotation = this.rotationState * this._rotation;
    },

    init(location,type,aisle,powerOn,target)
    {
        this.type = type;
        this.location = location;
        this.target = target;

        this.aisle =  aisle;

        this.powerOn = powerOn;

        if(type === 'A') {
            // 空白 暂时无需处理
        }else if(type === 'B') {
            // 电源 画实心圆
            this.setSolidCircle(0,0,cc.Color.GREEN);
        }else if(type === 'C') {
            // 灯 画空心圆 通电=>断电, 灰=>红
            var colorCircle = cc.Color.GRAY;
            if(this.powerOn) {
                colorCircle = cc.Color.RED;
            }
            this.setHollowCircle(0,0,colorCircle);
        }else if(type === 'D') {
            this.constant = false;
        }

        // 画电路
        this.setPowerLine(aisle);
    },
   
    callback: function (event) {
        if(this.constant) return;

        cc.log('当前点击位置',this.location);

        // 对应转换通道集合并保存到全局变量中
        var tempArr = this.aisle.split("")
        var temp = tempArr[3];
        tempArr.pop();
        tempArr.unshift(temp);
        this.aisle = tempArr.join("");

        dataArray[this.location.y].splice(this.location.x,1,this.type + '-' + this.aisle)

        if(this.target)
        {
            this.target.startSearch(this.location);
        }
        
    },
    // 通过传入 通道参数值画线
    setPowerLine(aisle)
    {   
        var colorLine = cc.Color.GRAY;
        if(this.powerOn) {
            colorLine = cc.Color.GREEN;
        }


        var aisleArray = aisle.split("");
        var tempArr = this.searchEqualIndexArr(aisleArray);

        var UP = undefined;
        var RIGHT = undefined;
        var DOWN = undefined;
        var LEFT = undefined;

        var tag = true;
        for(var i = 0; i < tempArr.length; i++)
        {
            var strValue = tempArr[i];

            var tempArray = strValue.split("-");

            var value = tempArray[0];
            var strIndex = tempArray[1];
            var indexArr = strIndex.split("");

            if(value != 0)
            {
                for(var j = 0; j < indexArr.length; j++)
                {
                    switch (parseInt(indexArr[j])) {
                        case AISLE.UP:
                            UP = cc.p(0, this.node.height / 2);
                            break;
                        case AISLE.RIGHT:
                            RIGHT = cc.p(this.node.width / 2, 0);
                            break;     
                        case AISLE.DOWN:
                            DOWN = cc.p(0, -this.node.height /2);
                            break;
                        case AISLE.LEFT:
                            LEFT = cc.p(-this.node.width/2, 0);
                            break;          
                        default:
                            break;
                    }
                }
                if(indexArr.length === 1)
                {
                    if(UP) {
                        this.setLine(true,colorLine,UP)
                    }else if(RIGHT){
                        this.setLine(true,colorLine,RIGHT)
                    }else if(DOWN){
                        this.setLine(true,colorLine,DOWN)
                    }else if(LEFT){
                        this.setLine(true,colorLine,LEFT)
                    }
                }else if(indexArr.length === 2){
                    if(UP && RIGHT) {
                        this.setLine(tag,colorLine,UP,RIGHT)
                    }else if(UP && DOWN) {
                        // 对角相连
                        this.setLine(tag,colorLine,UP,DOWN)
                    }else if(UP && LEFT) {
                        this.setLine(tag,colorLine,UP,LEFT)
                    }else if(RIGHT && DOWN) {
                        this.setLine(tag,colorLine,RIGHT,DOWN)
                    }else if(RIGHT && LEFT) {
                        // 对角相连
                        this.setLine(tag,colorLine,RIGHT,LEFT)
                    }else if(DOWN && LEFT) {
                        this.setLine(tag,colorLine,DOWN,LEFT)
                    }
                }else if(indexArr.length === 3){
                    if(!UP) {
                        this.setLine(true,colorLine,RIGHT,DOWN,LEFT)
                    }else if(!RIGHT){
                        this.setLine(true,colorLine,UP,DOWN,LEFT)                    
                    }else if(!DOWN){
                        this.setLine(true,colorLine,UP,RIGHT,LEFT)                    
                    }else if(!LEFT){
                        this.setLine(true,colorLine,UP,RIGHT,DOWN)                    
                    }
                }else if(indexArr.length === 4){
                    this.setLine(true,colorLine,UP,RIGHT,DOWN,LEFT)
                }
                tag = false;
                var UP = undefined;
                var RIGHT = undefined;
                var DOWN = undefined;
                var LEFT = undefined;
            }
        }

        // var UP = cc.p(0, this.node.height / 2);
        // var RIGHT = cc.p(this.node.width / 2, 0);
        // var DOWN = cc.p(0, -this.node.height /2);
        // var LEFT = cc.p(-this.node.width, 0);
    },

    // 画直线
    setLine(clear,color,pointUP,pointRIGHT,pointDOWN,pointLEFT)
    {
        if(clear) {
            this.ctx.clear();
        }
        
        this.ctx.lineWidth = 5;
        this.ctx.strokeColor = color; // cc.Color.GREEN

        switch (arguments.length) {
            case 3:
                this.ctx.moveTo(0,0);
                this.ctx.lineTo(pointUP.x,pointUP.y);
                break;
            case 4:
                this.ctx.moveTo(pointUP.x,pointUP.y);
                this.ctx.lineTo(pointRIGHT.x,pointRIGHT.y);
                break;
            case 5:
                this.ctx.moveTo(0,0);
                this.ctx.lineTo(pointUP.x,pointUP.y);
                this.ctx.moveTo(0,0);
                this.ctx.lineTo(pointRIGHT.x,pointRIGHT.y);
                this.ctx.moveTo(0,0);
                this.ctx.lineTo(pointDOWN.x,pointDOWN.y);
                break;
            case 6:
                this.ctx.moveTo(0,0);
                this.ctx.lineTo(pointUP.x,pointUP.y);
                this.ctx.moveTo(0,0);
                this.ctx.lineTo(pointRIGHT.x,pointRIGHT.y);
                this.ctx.moveTo(0,0);
                this.ctx.lineTo(pointDOWN.x,pointDOWN.y);
                this.ctx.moveTo(0,0);
                this.ctx.lineTo(pointLEFT.x,pointLEFT.y);
                this.ctx.circle(0,0, 5);

                break;
            default:
                break;
        }

        this.ctx.stroke();  
    },
    
    // 画实心圆 - 电源 绿 cc.Color.GREEN
    setSolidCircle(x,y,color)
    {
        this.ctxType.clear();
        this.ctxType.circle(x,y, 20);
        this.ctxType.fillColor = color;
        this.ctxType.fill();
    },

    // 画空心圆 - 灯 灰=>红 GRAY=>RED
    setHollowCircle(x,y,color)
    {
        this.ctxType.clear();
        this.ctxType.circle(x,y, 20);
        this.ctxType.strokeColor = color;
        this.ctxType.stroke();
    },
    
    // 查找数组中 相同元素 的下标集合并返回 ,返回格式为 ['a-1346'] 
    // a表示元素值,后边表示下标 (单个元素也需要返回)
    searchEqualIndexArr(arr)
    {
        var list = this.unique(arr);
        var array = [];

        for (var k = 0; k < list.length; k++)     
        {
            var _index = '';
            for (var i = 0; i < arr.length; i++)
            {
                if (arr[i] == list[k])
                {
                    _index += i;
                }
            }
            cc.log("数组中值为"+list[k]+"相同元素的下标为"+_index);
            array.push(list[k] + '-' + _index);
        }
        
        return array;
    },

    // 数组去重
    unique(array){
        var temp = []; 
        for(var i = 0; i < array.length; i++){
            if(temp.indexOf(array[i]) == -1){
                temp.push(array[i]);
            }
        }
        return temp;
    },
});

